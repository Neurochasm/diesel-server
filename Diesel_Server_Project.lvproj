﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{33C3E0EE-A771-4182-B113-E2A5B8BB281D}" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_SantaFe.lvlib/DATALOG_SANTAFE</Property>
	<Property Name="varPersistentID:{BFC9A7C8-8A44-48B7-A4BD-D6526B85C09D}" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_Terminal.lvlib/DATALOG_TERMINAL</Property>
	<Property Name="varPersistentID:{FF75E4FB-928E-4972-9975-C01EAE322553}" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_Pipa_Movil.lvlib/DATALOG_PIPA</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Controller" Type="Folder">
			<Item Name="controls" Type="Folder"/>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="Controller.lvlib" Type="Library" URL="../RT/Controller/Controller.lvlib"/>
		</Item>
		<Item Name="Timer" Type="Folder">
			<Item Name="controls" Type="Folder"/>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="Timer.lvlib" Type="Library" URL="../RT/Timer/Timer.lvlib"/>
		</Item>
		<Item Name="Stop" Type="Folder">
			<Item Name="Stop.lvlib" Type="Library" URL="../RT/Stop/Stop.lvlib"/>
		</Item>
		<Item Name="CheckPLCs" Type="Folder">
			<Item Name="CheckPLCs.lvlib" Type="Library" URL="../RT/CheckPLCs/CheckPLCs.lvlib"/>
		</Item>
		<Item Name="ReadVariables" Type="Folder">
			<Item Name="ReadVariables.lvlib" Type="Library" URL="../RT/ReadVariables/ReadVariables.lvlib"/>
		</Item>
		<Item Name="Alarms" Type="Folder">
			<Item Name="Alarms.lvlib" Type="Library" URL="../RT/Alarms/Alarms.lvlib"/>
		</Item>
		<Item Name="Transmission" Type="Folder">
			<Item Name="Transmission.lvlib" Type="Library" URL="../RT/Transmission Folder/Transmission.lvlib"/>
		</Item>
		<Item Name="DBProcess" Type="Folder">
			<Item Name="DBProcess.lvlib" Type="Library" URL="../RT/DBProcess/DBProcess.lvlib"/>
		</Item>
		<Item Name="Initialize" Type="Folder">
			<Item Name="InitializeSystem.vi" Type="VI" URL="../RT/Initialize/InitializeSystem.vi"/>
		</Item>
		<Item Name="Shared" Type="Folder">
			<Item Name="controls" Type="Folder">
				<Item Name="SystemQnames.ctl" Type="VI" URL="../RT/Shared/controls/SystemQnames.ctl"/>
			</Item>
			<Item Name="CronScheduler" Type="Folder">
				<Item Name="controls" Type="Folder"/>
				<Item Name="subVIs" Type="Folder"/>
				<Item Name="CRON.lvlib" Type="Library" URL="../RT/Shared/CronScheduler/CRON.lvlib"/>
			</Item>
			<Item Name="VGFs" Type="Folder">
				<Item Name="GV_InfoPipa.vi" Type="VI" URL="../RT/GFV/GV_InfoPipa.vi"/>
				<Item Name="GV_InfoSF.vi" Type="VI" URL="../RT/GFV/GV_InfoSF.vi"/>
				<Item Name="GV_InfoT.vi" Type="VI" URL="../RT/GFV/GV_InfoT.vi"/>
				<Item Name="GV_setpoints.vi" Type="VI" URL="../RT/GFV/GV_setpoints.vi"/>
				<Item Name="GV_status.vi" Type="VI" URL="../RT/GFV/GV_status.vi"/>
			</Item>
		</Item>
		<Item Name="Datalog" Type="Folder">
			<Item Name="controls" Type="Folder"/>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="Datalog.lvlib" Type="Library" URL="../RT/Datalog/Datalog.lvlib"/>
		</Item>
		<Item Name="PQ_Analysis" Type="Folder">
			<Item Name="controls" Type="Folder"/>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="PQ_Analysis.lvlib" Type="Library" URL="../RT/PQ_Analysis/PQ_Analysis.lvlib"/>
		</Item>
		<Item Name="Template" Type="Folder">
			<Item Name="controls" Type="Folder"/>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="Process Template.lvlib" Type="Library" URL="../RT/Template/Process Template.lvlib"/>
		</Item>
		<Item Name="UI Communication" Type="Folder">
			<Item Name="controls" Type="Folder">
				<Item Name="Global - RT Stream Connections.vi" Type="VI" URL="../RT/UI Communication/controls/Global - RT Stream Connections.vi"/>
				<Item Name="Message Data.ctl" Type="VI" URL="../RT/UI Communication/controls/Message Data.ctl"/>
			</Item>
			<Item Name="main" Type="Folder"/>
			<Item Name="subVIs" Type="Folder"/>
			<Item Name="UI Comunication.lvlib" Type="Library" URL="../RT/UI Communication/UI Comunication.lvlib"/>
		</Item>
		<Item Name="Shared_variables_diesel_server" Type="Folder">
			<Item Name="Server_Library_Pipa_Movil.lvlib" Type="Library" URL="../RT/Shared/Server_Library_Pipa_Movil.lvlib"/>
			<Item Name="Server_Library_SantaFe.lvlib" Type="Library" URL="../RT/Shared/Server_Library_SantaFe.lvlib"/>
			<Item Name="Server_Library_Terminal.lvlib" Type="Library" URL="../RT/Shared/Server_Library_Terminal.lvlib"/>
		</Item>
		<Item Name="Main_RT.vi" Type="VI" URL="../RT/Main_RT.vi"/>
		<Item Name="SystemStaticGlobals.vi" Type="VI" URL="../RT/SystemStaticGlobals.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="RTEH Classification to String.vi" Type="VI" URL="/&lt;userlib&gt;/_SEH/RTEH Classification to String.vi"/>
				<Item Name="RTEH Error Display Options.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/RTEH/RTEH Error Display Options.ctl"/>
				<Item Name="RTEH Error Notification Data.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/RTEH/RTEH Error Notification Data.ctl"/>
				<Item Name="RTEH Error Notification.vi" Type="VI" URL="/&lt;userlib&gt;/_SEH/Support VIs/RTEH/RTEH Error Notification.vi"/>
				<Item Name="RTEH Error Options.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/RTEH/RTEH Error Options.ctl"/>
				<Item Name="RTEH Error Processor.vi" Type="VI" URL="/&lt;userlib&gt;/_SEH/Support VIs/RTEH/RTEH Error Processor.vi"/>
				<Item Name="RTEH Shared Error Options.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/RTEH/RTEH Shared Error Options.ctl"/>
				<Item Name="RTEH Status Type.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/RTEH/RTEH Status Type.ctl"/>
				<Item Name="SEH Error Notification Command.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/Shared/SEH Error Notification Command.ctl"/>
				<Item Name="SEH Error Notification Config.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/Shared/SEH Error Notification Config.ctl"/>
				<Item Name="SEH Transmit Error Function.ctl" Type="VI" URL="/&lt;userlib&gt;/_SEH/Controls/Shared/SEH Transmit Error Function.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="AcqRead(Wfm).vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/AcqRead(Wfm).vi"/>
				<Item Name="AMC.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/AMC/AMC.lvlib"/>
				<Item Name="BufferCfg.vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/BufferCfg.vi"/>
				<Item Name="ChannelConfig.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/ChannelConfig.ctl"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="ConfigTiming.vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/ConfigTiming.vi"/>
				<Item Name="CreateChan.vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/CreateChan.vi"/>
				<Item Name="DynamicFPGAref.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/DynamicFPGAref.ctl"/>
				<Item Name="ep_AI Data Channel.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/Electrical Power/PQ/Common/Controls/ep_AI Data Channel.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="EventBuffer.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Event Logger Library/EventBuffer/EventBuffer.lvclass"/>
				<Item Name="EventLog.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Event Logger Library/EventLog/EventLog.lvclass"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="LogFile CSV.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Event Logger Library/LogFile CSV/LogFile CSV.lvclass"/>
				<Item Name="LogFile Generic.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Event Logger Library/LogFile Generic/LogFile Generic.lvclass"/>
				<Item Name="LogFile TDMS.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Event Logger Library/LogFile TDMS/LogFile TDMS.lvclass"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="Pound.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Software Watchdog/Pound/Pound.lvclass"/>
				<Item Name="Read(Poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/Read(Poly).vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="RT LEDs.vi" Type="VI" URL="/&lt;vilib&gt;/rtutility.llb/RT LEDs.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Start.vi" Type="VI" URL="/&lt;vilib&gt;/NI/cRIO Wfm/Start.vi"/>
				<Item Name="Tools_KeyedArray.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/Tools/Keyed Array/Tools_KeyedArray.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Watchdog Whack.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Whack.vi"/>
				<Item Name="Watchdog.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/Software Watchdog/Watchdog/Watchdog.lvclass"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="LabVIEWSMTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/smtpClient/LabVIEWSMTPClient.lvlib"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
			</Item>
			<Item Name="Blink.lvlib" Type="Library" URL="../RT/Blink/Blink.lvlib"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="PQ_Acquire.lvlib" Type="Library" URL="../RT/PQ_Acquire/PQ_Acquire.lvlib"/>
			<Item Name="Trigger.lvlib" Type="Library" URL="../RT/Trigger/Trigger.lvlib"/>
			<Item Name="Watchdog.lvlib" Type="Library" URL="../RT/Watchdog/Watchdog.lvlib"/>
			<Item Name="OpenDBdiesel.vi" Type="VI" URL="../../Diesel_Server_V0.0.1/Resources/OpenDBdiesel.vi"/>
			<Item Name="AlarmaCorreo.vi" Type="VI" URL="../../Diesel_Server_V0.0.1/Resources/AlarmaCorreo.vi"/>
			<Item Name="UpdateAlarmas.vi" Type="VI" URL="../../Diesel_Server_V0.0.1/Resources/UpdateAlarmas.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="DieselServer" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{BC81757D-9FFF-44F2-A2F2-53CCF97A98C8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{B47954CD-3FFA-4108-BEA2-0B71AEFA15C7}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{5D19B98A-D424-40D2-8EBC-9E08287C5312}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DieselServer</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Diesel_Server/ServerDiesel</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4874E351-E88B-4C76-AFCE-8AC5E7D019A2}</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DieselServer.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Diesel_Server/ServerDiesel/DieselServer.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Diesel_Server/ServerDiesel/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_Vardep[0].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[0].LibItemID" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_Pipa_Movil.lvlib</Property>
				<Property Name="Exe_Vardep[1].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[1].LibItemID" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_SantaFe.lvlib</Property>
				<Property Name="Exe_Vardep[2].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[2].LibItemID" Type="Ref">/My Computer/Shared_variables_diesel_server/Server_Library_Terminal.lvlib</Property>
				<Property Name="Exe_VardepDeployAtStartup" Type="Bool">true</Property>
				<Property Name="Exe_VardepLibItemCount" Type="Int">3</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{309A5477-EF32-430D-8E4C-FB46D5FCE1A2}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Main_RT.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[10].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Alarms</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">Container</Property>
				<Property Name="Source[11].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[11].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Transmission</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Container</Property>
				<Property Name="Source[12].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[12].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/DBProcess</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">Container</Property>
				<Property Name="Source[13].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[13].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Shared_variables_diesel_server</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[13].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Controller</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Initialize</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Shared</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[5].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Timer</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">Container</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/SystemStaticGlobals.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[7].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Stop</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="Source[8].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[8].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/CheckPLCs</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Container</Property>
				<Property Name="Source[9].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[9].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/ReadVariables</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">14</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DieselServer</Property>
				<Property Name="TgtF_internalName" Type="Str">DieselServer</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 </Property>
				<Property Name="TgtF_productName" Type="Str">DieselServer</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{69D6C72E-F0ED-4217-8F19-7E142866621A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DieselServer.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
